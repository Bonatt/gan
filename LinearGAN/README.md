# Linear GAN

I generated some horizontal and vertical lines as a dataset, and trained a 
generator/discriminator pair (GAN, here) to create and identify horizontal and 
vertical lines. This was a tangent to an 
analogue [ClockReader](https://gitlab.com/Bonatt/clockreader) I was developing; 
the goal was the generate a dataset for handset regression instead of applying 
traditional computer vision/math techniques. But I got distracted, of course.
I implemented a few GAN architectures that I read in some papers, at least.

Documention to continue.
